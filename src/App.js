import { useState } from "react";

import "./App.css";
import Expenses from "./components/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

const App = () => {
  const [expenses, setExpenses] = useState([
    {
      id: "e1",
      title: "Car Insurance",
      amount: 200,
      date: new Date(2019, 3, 23),
    },
    {
      id: "e2",
      title: "Life Insurance",
      amount: 500,
      date: new Date(2020, 9, 23),
    },
    {
      id: "e3",
      title: "Jewellary Repair",
      amount: 20,
      date: new Date(2020, 2, 31),
    },
    { id: "e4", title: "Travel", amount: 10, date: new Date(2022, 3, 23) },
  ]);

  const saveNewExpenseHandler = (data) => {
    // const expensesCopy = [...expenses];
    // expensesCopy.push(data);
    setExpenses((prevState) => {
        return [data, ...expenses]
    });
  };

  return (
    <div className="App">
      <NewExpense onSaveNewExpense={saveNewExpenseHandler} />
      <Expenses expenses={expenses} />
    </div>
  );
};

export default App;
