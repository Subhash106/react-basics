import ExpenseDate from "./ExpenseDate";
import "./ExpenseItem.css";
import Card from "./Card";
import { useState } from "react";

const ExpenseItem = (props) => {
  let amount = props.amount;
  const [title, setTitle] = useState(props.title);

  const clickHandler = () => {
    setTitle("Updated Title");
    amount = 200;
    console.log(title);
    console.log(amount);
  };

  return (
    <Card className="expense-item">
      <ExpenseDate date={props.date} />
      <div className="expense-item__description">
        <h2>{title}</h2>
        <div className="expense-item__price">Rs. {amount}</div>
      </div>
      <button onClick={clickHandler}>Change Title</button>
    </Card>
  );
};

export default ExpenseItem;
