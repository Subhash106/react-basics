import ExpenseItem from "./ExpenseItem";
import Card from "./Card";
import "./Expenses.css";
import FilterExpense from "./FilterExpense/FilterExpense";
import { useState } from "react";
import ExpenseChart from './ExpenseChart';

const Expenses = (props) => {
  const [selectedYear, setSelectedYear] = useState("2020");

  const changeYearHandler = (selectedYear) => {
    setSelectedYear(selectedYear);
  };

  const filteredExpense = props.expenses.filter(
    (expense) => expense.date.getFullYear().toString() === selectedYear
  );

  let expensesContent = (
    <p style={{ color: "white", fontWeight: "bold" }}>No Expense Found!</p>
  );

  if (filteredExpense.length > 0) {
    expensesContent = filteredExpense.map((expense) => (
      <ExpenseItem
        key={expense.id}
        title={expense.title}
        amount={expense.amount}
        date={expense.date}
      />
    ));
  }

  return (
    <Card className="expenses">
      <ExpenseChart expenses={filteredExpense} />
      <FilterExpense
        selectedYear={selectedYear}
        onChangeYear={changeYearHandler}
      />
      {expensesContent}
    </Card>
  );
};

export default Expenses;
