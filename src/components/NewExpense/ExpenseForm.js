import React, { useState } from "react";
import "./ExpenseForm.css";

const ExpenseForm = (props) => {
  const [title, setTitle] = useState("");
  const [amount, setAmount] = useState("");
  const [date, setDate] = useState("");

  //   const [userInput, setUserInput] = useState({
  //       title: '',
  //       amount: '',
  //       date: ''
  //   });

  const titleChangedHandler = (event) => {
    setTitle(event.target.value);
    // setUserInput({
    //     ...userInput,
    //     title: event.target.value
    // })

    // setUserInput((prevState) => {
    //     return {...prevState, title: event.target.value}
    // })
  };

  const amountChangedHandler = (event) => {
    setAmount(event.target.value);
    // setUserInput({
    //     ...userInput,
    //     amount: event.target.value
    // })

    // setUserInput((prevState) => {
    //     return {...prevState, amount: event.target.value}
    // })
  };

  const dateChangedHandler = (event) => {
    setDate(event.target.value);
    // setUserInput({
    //     ...userInput,
    //     date: event.target.value
    // })

    // setUserInput((prevState) => {
    //     return {...prevState, date: event.target.value}
    // })
  };

  const formSubmitHandler = (event) => {
    event.preventDefault();

    const formData = {
      title: title,
      amount: +amount,
      date: new Date(date),
    };

    props.saveExpenseData(formData);

    setTitle("");
    setAmount("");
    setDate("");
  };

  const cancelButtonHandler = () => {
    props.onCancelButtonClick();
  }

  return (
    <form onSubmit={formSubmitHandler}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Title</label>
          <input type="text" value={title} onChange={titleChangedHandler} />
        </div>
        <div className="new-expense__control">
          <label>Amount</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            value={amount}
            onChange={amountChangedHandler}
          />
        </div>
        <div className="new-expense__control">
          <label>Date</label>
          <input
            type="date"
            min="2019-01-01"
            max="2022-12-31"
            value={date}
            onChange={dateChangedHandler}
          />
        </div>
        <div className="new-expense__actions">
          <button onClick={cancelButtonHandler}>Cancel</button>
          <button type="submit">Add Expense</button>
        </div>
      </div>
    </form>
  );
};

export default ExpenseForm;
