import React, { useState } from "react";
import "./NewExpense.css";
import ExpenseForm from "./ExpenseForm";

const NewExpense = (props) => {
  const saveExpenseData = (formData) => {
    const data = {
      ...formData,
      id: Math.random().toString(),
    };

    props.onSaveNewExpense(data);
  };

  const [isEditing, setIsEditing] = useState(false);

  const showFormHandler = () => {
    setIsEditing(true);
  };

  const onCancelButtonClickHandler = () => {
    setIsEditing(false);
  };

  return (
    <div className="new-expense">
      {!isEditing && <button onClick={showFormHandler}>Add New Expense</button>}
      {isEditing && (
        <ExpenseForm
          onCancelButtonClick={onCancelButtonClickHandler}
          saveExpenseData={saveExpenseData}
        />
      )}
    </div>
  );
};

export default NewExpense;
