import React from "react";
import "./FilterExpense.css";

const FilterExpense = (props) => {
  const changeYearHandler = (event) => {
    const selectedYear = event.target.value;

    props.onChangeYear(selectedYear);
  };

  return (
    <div className="expenses-filter">
      <div className="expenses-filter__control">
        <label>Filter Expense By Year</label>
        <select value={props.selectedYear} onChange={changeYearHandler}>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
        </select>
      </div>
    </div>
  );
};

export default FilterExpense;
